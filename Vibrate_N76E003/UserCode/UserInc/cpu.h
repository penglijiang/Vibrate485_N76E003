#ifndef _CPU_H_
#define _CPU_H_
////////////////////////////////////
////////////////////////////////////

//1.调用新唐官方头文件 . 
 
/* 加载官方头文件 */
#include "N76E003.h"
#include "Common.h"
#include "Delay.h"
#include "SFR_Macro.h"
#include "Function_define.h"

 
 
////////////////////////////////////////////////
//2.定义可移植数据类型
//----------------------------8 bit
typedef unsigned char           u08  ;
typedef unsigned char           Uint8;
typedef unsigned char           uint8;
typedef unsigned char           u8	 ;
typedef unsigned char           U8	 ;
//typedef unsigned char           UINT8;
typedef unsigned char           uchar;
typedef  signed  char		    int8 ;

//----------------------------16 bit
typedef unsigned short int 	    u16	 ;
typedef unsigned short int 	    U16   ;
typedef unsigned short int 	    uint16;
typedef unsigned short int 	    Uint16;
//typedef unsigned short int 	    UINT16;
typedef  		 short int 	    int16 ;
typedef 	     short int 	    INT16 ;
 
//----------------------------32 bit
typedef unsigned int            u32   ; 
typedef unsigned int            uint32;
typedef unsigned int            U32	  ;
//typedef unsigned int            UINT32;
            
typedef  signed int             INT32 ;
typedef  signed int  	int32;
typedef void (*FUNPTR_VOID_VOID )( void );//定义返回和参数均为void的函数
#ifndef NULL
	#define NULL 0
#endif//----------------------------bool
//
#ifndef BOOL
	#define BOOL u08
#endif
#ifndef bool
	#define bool u08
#endif
//true
#if !defined( ON) || !defined(on)
	#define ON 1
	#define on 1
#endif

#if !defined( OK) || !defined(ok)
	#define OK 1
	#define ok 1
#endif


#if !defined( TRUE) || !defined(true)|| !defined(True)
	#define TRUE 1
	#define true 1
	#define True 1
#endif

#if !defined (YES ) || !defined (yes)
	#define YES 1
	#define yes 1
#endif 

//not true

#if !defined( OFF) || !defined(off)
	#define OFF 0
	#define off 0
#endif 
#if !defined(FALSE) || !defined( false)
	#define FALSE 0
	#define false 0
#endif
#if !defined( NO ) || !defined (no)
	#define NO 0
	#define no 0
#endif 

#if !defined( NOK) || !defined(nok)
	#define NOK 0
	#define nok 0
#endif

//-----------------------------------DIRECTION
#define   IN 0
#define   OUT 1
///////////////////////////////////////////////
//3. avr 系列单片机位操作宏
typedef struct
 {
    unsigned char bit0:1 ; 
    unsigned char bit1:1 ;
    unsigned char bit2:1 ;
    unsigned char bit3:1 ;
    unsigned char bit4:1 ;
    unsigned char bit5:1 ;
    unsigned char bit6:1 ;
    unsigned char bit7:1 ;
}bit_field;

#define GET_BIT(adr) 		(*(( volatile bit_field * )(&adr)))



#define cli()   EA=0;
#define sei()   EA =1;
 

#endif

