/*
*    文件名:   config.h
*    功能:     系统统一头文件   
*/
#ifndef _CONFIG_H
#define _CONFIG_H

/* 0- 定义软硬件版本 */
#define HVERSION_V10 0
#define HVERSION_V11 1
#define HVERSION   HVERSION_V11  

#define SVERSION 11



/* 1- 包含本CPU的支持文件  */
#include "cpu.h"

/* 2- 包含驱动.应用头文件 */

#include "DRV_wdt.h"
#include "DRV_gpio.h"
#include "DRV_spi.h"
#include "DRV_led.h"
#include "DRV_usart0.h"
#include "DRV_dataflash.h"

#include "initialization.h"
#include "Vibrate485.h"
#include "task_var.h"
#include "task_modbus.h"
#include "ADXL345.h"

 

    
    

		  
/////////////////////////////////////////========>2系统常见常数定义
/*超时时间*/
#define WAIT_TICKS 5000
/**/

/* 系统运行状态定义*/	
#define RUN 0
#define MENU 1
#define EDIT 2

 /*传感器路数定义*/
#define CH1	0
#define	CH2	1
#define CH3	2
#define	CH4	3

/*温湿度采样相关*/

/////////////////////////////////////////========>3 配置系统时间所用定时器的定时间隔


/////////////////////////////////////////========>4 本系统可能用到的16位数的数据模式

#define HIGHBYTE(UINT16)  ( ((unsigned char *)(&UINT16))[1] )
#define LOWBYTE(UINT16)   ( ((unsigned char *)(&UINT16))[0] )
#define HIGH_(u16)        (* ((u08 *)(&u16) +0))
#define LOW_(u16)         (* ((u08 *)(&u16) +1))
#define DIM(array,type)   (sizeof(array)/sizeof(type))
/////////////////////////////////////////========>5 定时器设置
#define TIMER_N  2 						//软定时序号
#define TMR0   0 						//软定时序号
#define TMR1   1 						//软定时序号
#define TMR2   2 						//软定时序号
#define TMR3   3 						//软定时序号
#define TMR4   4 						//软定时序号
#define TMR5   5 						//软定时序号
#define TMR6   6 						//软定时序号
 
 
/////////////////////////////////////////========>6 定义软件定时器结构 
//每个定时器只占用2个字节 
//各命令字的含义如下：基础的定时单位以秒为单位。实际的计时时间为
//Tminute x 60 + Tsecond (秒)
//second不能为0；为0，则
////////////////////////////////////////////////////////////////////
struct	TIMER
{ 
      u08 TR ;        			//起停标志，0－停止运行，1－开始运行 
      u08 TF ;        			//溢出标志，0－未溢出，1－溢出 
      u08 second ;    			//6位定时值，秒针控制 最大2^6-1=63 
      u08 minute ;    			//8位定时值，分针控制 最大2^8-1=255 
	  FUNPTR_VOID_VOID funptr;	//溢出时执行的函数指针
}	;
////////////////////////////////////////////////////////////////////
//定时器相关操作函数
void set_on_timer	( u08 timer ,u08 minute,u08 second, FUNPTR_VOID_VOID fptr	);
void set_off_timer	( u08 timer  												);
void on_timer		( void														);
//各个定时器溢出时需要执行的函数



//与库函数配合使用的两个软件延时函数
void _delay_100ms( void );
void _delay_500ms( void );
void _delay_1s  ( void );
void test_led   ( void );
void _delay_us( u16 n_us);
void _delay_ms( uint32_t n_ms );

//定时器相关控制变量
extern u08 f10ms 	  ;	//定时器到标志
extern u08 f20ms 	  ;	//定时器到标志
extern u08 f10ms 	  ;	//定时器到标志
extern u08 f1s 	  ;	    //定时器到标志
/////////////////////////////////////////========>7全局变量声明
 
/* 函数声明 */
void  Timer0Init(void);
void  Timer2Init(void);





/* 全局常用函数声明 */
 




#endif

