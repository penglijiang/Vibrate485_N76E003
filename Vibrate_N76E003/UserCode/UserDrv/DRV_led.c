/*---------------------------------------------------------------------------------------------------------*/
/*                                                                                                         */
/* Copyright(c) 2017 Nuvoton Technology Corp. All rights reserved.                                         */
/*                                                                                                         */
/*---------------------------------------------------------------------------------------------------------*/
 
//***********************************************************************************************************
//  File Function:  振动测量主程序
//***********************************************************************************************************

/* Includes ------------------------------------------------------------------*/
#include "config.h"

/**
  * @brief LED_proc 指示灯处理 
  *         
  * @note  处理led的闪烁 ,点亮;
  *         
  * @retval None
  */
void LED_proc( void )
{
    /* 闪烁打开 */
    if( SYS_led1_flash == ON )
    {
        if( SYS_led1 == ON )
        {
            WORK_LED =~WORK_LED;
        }
        else
        {
            WORK_LED = LED_OFF ;
        }
    }
    /* 闪烁关闭 */
    else
    {
        if(SYS_led1 == ON )  
        {
            WORK_LED = LED_ON;
        }
        else 
        {
            WORK_LED = LED_OFF;
        }
    }
}

/**
  * @brief LED_open 指示灯处理 
  *         
  * @note  处理led的闪烁 ,点亮;
  *         
  * @retval None
  */
void LED_operate( u08 led_index, u08 led_cmd )
{
    if( led_index == _LED1_ )
    {
        SYS_led1 = led_cmd;
    }
    else
    if( led_index == _LED2_ )
    {
        SYS_led2 = led_cmd;
    }
}

/**
  * @brief LED_open 指示灯处理 
  *         
  * @note  处理led的闪烁 ,点亮;
  *         
  * @retval None
  */
void LED_flash( u08 led_index, u08 led_cmd )
{
    if( led_index == _LED1_ )
    {
        SYS_led1 = led_cmd;
    }
    else
    if( led_index == _LED2_ )
    {
        SYS_led2 = led_cmd;
    }
}
