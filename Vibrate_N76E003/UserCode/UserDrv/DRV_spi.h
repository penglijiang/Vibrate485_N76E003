#ifndef _DRV_SPI_H
#define _DRV_SPI_H 


 
void SPI_Initial(void);
void ReadFromADXL345ViaSpi(unsigned char RegisterAddress, unsigned char NumberofRegisters, unsigned char *RegisterData);
void WriteToADXL345ViaSpi(unsigned char RegisterAddress, unsigned char NumberofRegisters, unsigned char *RegisterData);

#define SET_CS()		ADXL345_CS =1
#define CLR_CS()		ADXL345_CS =0 

#define	SET_SCL()		ADXL345_SCLK =1
#define	CLR_SCL()		ADXL345_SCLK =0

#define SET_SDO()		ADXL345_SDO = 1
#define CLR_SDO()		ADXL345_SDO = 0





#endif

