/****************************************************************************************
  Copyright (C), 1988-1999, Huawei Tech. Co., Ltd.
  File name:      usart.c
  Author:         Harvard
  Version:        0.0
  Date: 		20070620
  Description:  完成串行口的初始化，设置，提供单字节的发送接收
  History: 
  ----------------------------------------------------------------------------------------    
  1. Date:		20070622
     Author:	XXJ
     Modification:
	 			t1.5与t3.5采用固定间隔代替，分别为3
  2. ...
  ----------------------------------------------------------------------------------------    
  1. Date:		2016-03-03
     Author:  Shine
     Abstract: - 升级串口初始化函数,改串口晶振为外部12MHZ,波特率范围从4800-115200bps
               - 串口初始化i函数的参数从16bit升级到32bit
  
*****************************************************************************************/
#include "config.h"


 u16  _crc16_update(u16 crc, u08 a)
{
	int i;
	crc ^= a;
	for (i = 0; i < 8; ++i)
	{
	    if (crc & 1)
		crc = (crc >> 1) ^ 0xA001;
	    else
		crc = (crc >> 1);
	}
	return crc;
} 

/*
** 串行口初始化
** baudrate取值为: 4800 9600 19200
*/

void usart_init(u32 baudrate)
{
    /* 根据实际需要,选择不同的定时器作为波特率发生器 */
    InitialUART0_Timer3(baudrate);
    /* 将485引脚置为接收状态 */
    TXD_EN = 0;
    /* 使能串口中断0*/
    set_ES;
}

void usart_dis_recv(void)
{
    /* 禁止接收 */
    TXD_EN =1;
    /* 关闭串口0中断 ;*/
    clr_ES; 
}
void usart_en_recv (void)
{
    /* 使能接收引脚*/
    TXD_EN = 0;
    /* 打开串口中断*/
    set_ES;
}

/*超时定时器初始化及其开关控制*/
#define T2_TICK_MIN		1000000/(F_CPU/1024)
#define T2_TICK			1000	//1ms
#define TIMER2_BGN_VAL 	 (255-T2_TICK/T2_TICK_MIN)

//static u08 CNT_t15=0;			        //1.5字符时间
//static u08 CNT_t35=0;			        //3.5字符时间
static  u08 SYS_cnt_frame   =0;		    //时间间隔计数器，用于1.5 和3.5字符时间判断
        u08 SYS_t2_on       = OFF ;     //用于1.5 3.5字符判断的软定时器 开关. 



void timer2_on(void)
{
    
    SYS_cnt_frame = 0 ;
    SYS_t2_on = ON;
}
void timer2_off(void)
{
    SYS_t2_on = OFF;
    /** 帧间隔控制 */
    SYS_cnt_frame = 0;
//    CNT_t15= 0;
//    CNT_t35= 0;
}

/**
 * @brief       Timer-2 IRQ
 *
 * @param       None
 *
 * @return      None
 *
 * @details     The TIMER2 default IRQ, declared in startup_M051Series.s.
 */
 void Timer2_ISR (void) interrupt 3 
{
    //////////////////////////////////////////////--重新加载定时器初值
     /* 给TIMER 赋初值 
     ** 定时器时钟为:16MHZ/12= 4/3MHZ
     ** 定时器tick为: 1/ (4/3MHZ) = 3/4us = 0.75uS
     ** 定时器要定时1ms:
     ** 定时器初值为: 1000us/0.75uS = 1333
     */
    TH1 = (65535-1333)/256;
    TL1 = (65535-1333)%256;    


    /* 如果定时器打开 */
    if( SYS_t2_on == ON )
   {

          SYS_cnt_frame++;
          
          if ( SYS_cnt_frame > T35)	 //a frame was finished
          {
               usart_dis_recv();	  //禁止接收中断

               timer2_off ()	  ;   //tunr off timer;

               SYS_cnt_frame = 0 ;   //reset timer

               RxBufPtr--;			 //调整指针 	

               fFrameDone    = OK;   //frame is completed 
           }       
    }
    /* 定时器关,还未进行帧间隔判断 */
   else
   {
        SYS_cnt_frame = 0;
   }
   
}



u08 RxBuf[MAX_BUF_LEN]={0,1,2,3,4};
u08 TxBuf[MAX_BUF_LEN];
u08 RxBufPtr 	= RX_BUF_BGN_ADR;
u08 TxBufPtr 	= TX_BUF_BGN_ADR;
BOOL fIsRXING	= NO;          //is receiving now
BOOL fIsTXING	= NO;          //is transsending now
BOOL fRxBufFull	= NO;
BOOL fTxBufFull = NO;
BOOL fFrameDone = NO;		   //one frame finished 
BOOL fFrameBad  = NO;		   //frame was broken
u16  CRC		= 0xFFFF;	   //CRC初始值	


/*接收中断*/
void UART0_IRQHandler( void ) interrupt 4
{
    /* 1 - 接收中断处理 */ 
    if ( RI==1 ) 
    {                                       /* if reception occur                       */
        clr_RI;                             /* clear reception flag for next reception  */

        /* Rx trigger level is 1 byte*/    
       RxBuf[RxBufPtr] = SBUF;

        /* reset timer */
       SYS_cnt_frame = 0 ;   

        /*new frame coming*/ 
        if( RxBufPtr == RX_BUF_BGN_ADR )//新的一帧开始
        {
            CRC = 0xffff	;	            //准备crc计算，

            timer2_on();                    //开启定时器计时
        }

        /*with a frame and RxBuf is not full*/
        /*when we detect a frame was borken according to t15 ,
        * we keep on receiving in order to synchronize the frame  
        */
        else if
          ( RxBufPtr < MAX_BUF_LEN -1)
        {  
            if ( SYS_cnt_frame > T15 && SYS_cnt_frame < T35 ) 
            {
                 SYS_cnt_frame = 0 ;   //reset timer
            
                 fFrameBad     = TRUE; //frame is broken 
            }
        }

        /*RxBuf full */
        /* 
        **
        */
        else if( RxBufPtr >= MAX_BUF_LEN -1)
        {
            usart_dis_recv()    ;			 //stop receving
        
            timer2_off ()	    ;            //tunr off timer;

            fRxBufFull 	  = YES ;            //缓冲区已满
           
            fFrameBad     = TRUE;            //frame is broken 
           
            RxBufPtr --;		             //指针保持不变，防止破坏其他数据区
        } 
            CRC = _crc16_update( CRC,RxBuf[RxBufPtr] );

            RxBufPtr++;						//adjust pointer
    }//end while

    /* 2 - 发送标记处理 */ 
    if(TI==1)
    {
        clr_TI;                             /* if emission occur */
    }
}

/*查询发送*/
void usart_send_byte( u08 txbyte )
{
    TXD_EN      =1;
    WORK_LED    =0;
    /* 向串口写入1字节   */
    Send_Data_To_UART0( txbyte );
    /* 等待1字节传输完毕 */
    TXD_EN      =0;
    WORK_LED    =1;
}


 






 