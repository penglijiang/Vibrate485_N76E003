#ifndef _DRV_USART0_H
#define _DRV_USART0_H
 
//////////////////////
#define MAX_BUF_LEN	40	  /*transmit constants*/
#define RX_BUF_BGN_ADR	0
#define TX_BUF_BGN_ADR	0

	/*Gobal variables*/
extern u08  RxBuf[MAX_BUF_LEN];
extern u08  TxBuf[MAX_BUF_LEN];
extern u08  RxBufPtr  ;
extern u08  TxBufPtr  ;
extern BOOL fIsRXING  ;          //is receiving now
extern BOOL fIsTXING  ;          //is transsending now
extern BOOL fRxBufFull;
extern BOOL fTxBufFull;
extern BOOL fFrameDone;		     //one frame finished 
extern BOOL fFrameBad ;		     //frame was broken
extern u16 CRC;

 
void   usart_init ( u32 baudrate   );
void   timer2_on  ( void           );//turn on  t2
void   timer2_off ( void           );//turn off t2
void   usart_dis_recv( void        );
void   usart_en_recv ( void        );
void   usart_send_byte( u08 txbyte );


 u16  _crc16_update(u16 crc, u08 a);
 
 
 #define RS485_SEND0 P20

 

#endif


