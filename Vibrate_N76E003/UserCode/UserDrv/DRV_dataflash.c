/**
  ******************************************************************************
  * @file    DRV_dataflash.c
  * @author  Shine
  * @version V1.0
  * @date    2017-06-29
  * @brief   利用flash,模拟eeprom,作为掉电存储,全部设置为APROM,APROM的最后2K空间
  *          (3800-47FFh)
  ******************************************************************************
  * @attention
  *
  *
  ******************************************************************************
  */ 


/*---------------------------------------------------------------------------------------------------------*/
/*                                                                                                         */
/* Copyright(c) 2017 Nuvoton Technology Corp. All rights reserved.                                         */
/*                                                                                                         */
/*---------------------------------------------------------------------------------------------------------*/

//***********************************************************************************************************
//  Website: http://www.nuvoton.com
//  E-Mail : MicroC-8bit@nuvoton.com
//  Date   : Jan/21/2017
//***********************************************************************************************************

//***********************************************************************************************************
//  File Function: N76E003 APROM program DATAFLASH as EEPROM way 
//***********************************************************************************************************
#include "config.h"


/*****************************************************************************************************************
write_DATAFLASH_BYTE :
user can copy all this subroutine into project, then call this function in main.
******************************************************************************************************************/		
unsigned char xdata flashram[128];
void write_DATAFLASH_BYTE(UINT16 u16_addr,UINT8 u8_data)
{
	u16 looptmp=0;
    u08 u8_addrl_r;
	unsigned char code *cd_longaddr;
	u08 xdata *xd_tmp;
	
///////////Check page start address:检查要写入的地址,应该是0-128的pagesize

	u8_addrl_r = u16_addr;//Shine: 获取要写入地址的低8位,根据是否大于128 完成页面的强制对齐
	if (u8_addrl_r<0x80)
	{
		u8_addrl_r = 0;
	}
	else 
	{
		u8_addrl_r = 0x80;
	}
    
///////////Save APROM data to XRAM:
	//xd_tmp = 0x80;        /// 原来写法,直接利用xdata的0x80-0x100来作为缓存有风险,应该用固定内存
	xd_tmp = &flashram[0];  /// 改为由系统分配的临时缓存的首地址,当然,存在内存的资源浪费
	cd_longaddr = (u16_addr&0xff00)+u8_addrl_r;	///获取要写入的flash地址
	//while (xd_tmp !=0x100)
	while (xd_tmp !=(&flashram[0]+0x80) )///从基址开始依次读取128byte
	{
		*xd_tmp = *cd_longaddr;
		looptmp++;
		xd_tmp++;
		cd_longaddr++;
	}
    
/////////Modify customer data in XRAM
/// 官方的库写法,但是个人认为有隐患, 0x80的xram可能会被系统分配;
//	u8_addrl_r = u16_addr; ///获取写入地址,分: <128 ;和大于128两种情况
//	if (u8_addrl_r<0x80)   ///写入地址<128的,直接从xram的0x80,也就是128开始写入xram
//	{
//		xd_tmp = u8_addrl_r+0x80;
//	}
//	else                  ///大于128的,也是从128后的地址开始写入,只是不在需要+128的偏置.
//	{
//		xd_tmp = u8_addrl_r+0;
//	}
//	*xd_tmp = u8_data;///地址修正后,直接写入
    
	u8_addrl_r = u16_addr;
    
    /* 将写入地址0-256:变成0-128;因为xram混存仅为128*/
	if (u8_addrl_r<0x80)
	{
		xd_tmp =  &flashram[0] + u8_addrl_r;
	}
	else
	{
		xd_tmp =  &flashram[0] +(u8_addrl_r-0x80);
	}
	*xd_tmp = u8_data;
        
    
////////Erase APROM DATAFLASH page
		IAPAL = u16_addr;   ///获取要写入地址的低位和高位;
		IAPAH = u16_addr>>8;///
		IAPFD = 0xFF;
        set_IAPEN; 
        set_APUEN;
        IAPCN = 0x22; 		
        set_IAPGO; 
        
/////////Save changed RAM data to APROM DATAFLASH
//	u8_addrl_r = u16_addr;
//	if (u8_addrl_r<0x80)
//	{
//		u8_addrl_r =0;
//	}
//	else
//	{
//		u8_addrl_r = 0x80;
//	}
//        xd_tmp = 0x80;
//        IAPAL = u8_addrl_r;
//        IAPAH = u16_addr>>8;
//        set_IAPEN; 
//        set_APUEN;
//        IAPCN = 0x21;
//		while (xd_tmp !=0xFF)
//		{
//			IAPFD = *xd_tmp;
//			set_IAPGO;
//			IAPAL++;
//			xd_tmp++;
//		}
//        
    /* 128字节依次写入 flash */
	u8_addrl_r = u16_addr;
	if (u8_addrl_r<0x80)
	{
		u8_addrl_r =0;
	}
	else
	{
		u8_addrl_r = 0x80;
	}
        xd_tmp =   &flashram[0] ;
        IAPAL = u8_addrl_r;
        IAPAH = u16_addr>>8;
        set_IAPEN; 
        set_APUEN;
        IAPCN = 0x21;
		while (xd_tmp !=(&flashram[0]+0x80) )
		{
			IAPFD = *xd_tmp;
			set_IAPGO;
			IAPAL++;
			xd_tmp++;
		}
        
     
		clr_APUEN;
		clr_IAPEN;
}	
	
//-------------------------------------------------------------------------
UINT8 read_APROM_BYTE(UINT16 code *u16_addr)
{
	UINT8 rdata;
	rdata = *u16_addr>>8;
	return rdata;
}

/******************************************************************************************************************/	



//void main (void) 
//{
//		UINT8 datatemp;
///* -------------------------------------------------------------------------*/
///*  Dataflash use APROM area, please ALWAYS care the address of you code    */
///*	APROM 0x3800~0x38FF demo as dataflash 				      				      			*/
///* 	Please use Memory window key in C:0x3800 to check earse result					*/	      
///* -------------------------------------------------------------------------*/
//		InitialUART0_Timer1(115200);
////call write byte 
//		write_DATAFLASH_BYTE (0x3881,0x55);
//		write_DATAFLASH_BYTE (0x3882,0x56);
//		write_DATAFLASH_BYTE (0x3855,0xaa);
//		write_DATAFLASH_BYTE (0x3856,0x66);
////call read byte
//		datatemp = read_APROM_BYTE(0x3882);

//    while(1)
//		{
////				printf ("\n data temp = 0x%bx", datatemp);
//		}
//}








