#ifndef _DRV_LED_H
#define _DRV_LED_H

#define _LED1_  0
#define _LED2_  1
#define _LED3_  2
#define _LED4_  3
#define _LED5_  4

void LED_proc( void );
void LED_operate( u08 led_index, u08 led_cmd );
void LED_flash( u08 led_index, u08 led_cmd );


#endif


