/**
  ******************************************************************************
  * @file    DRV_wdt.c
  * @author  Shine
  * @version V1.0
  * @date    2017-06-29
  * @brief   看门狗功能
  ******************************************************************************
  * @attention
  *
  *
  ******************************************************************************
  */ 
/* 加载官方头文件 */
#include "N76E003.h"
#include "Common.h"
#include "Delay.h"
#include "SFR_Macro.h"
#include "Function_define.h"


/**
  * @brief 开启看门狗
  *         
  * @note  如果熔丝位中没有打开看门狗,可以通过本函数
  *        打开看门狗
  * @retval None
  */
void Enable_WDT_Reset_Config(void)
{
    set_IAPEN;
    IAPAL = 0x04;
    IAPAH = 0x00;
    IAPFD = 0x0F;
    IAPCN = 0xE1;
    set_CFUEN;
    set_IAPGO;                                   //trigger IAP
		while((CHPCON&SET_BIT6)==SET_BIT6);     //check IAPFF (CHPCON.6)
    clr_CFUEN;
    clr_IAPEN;
}

/**
  * @brief 看门狗初始化
  *         
  * @note  看门狗初始化,
  *         
  * @retval None
  */
void WDT_Initial( void )
{
    /* 使能看门狗的复位功能  */
    Enable_WDT_Reset_Config();

    /* 时效寄存器控制,解锁一下 */
    TA=0xAA;
    TA=0x55;
    
    /* WDT控制寄存器 */
    WDCON=0;
    set_WPS2; ///设置溢出间隔为409.6ms
    clr_WPS1; ///
    set_WPS0; ///
    set_WDCLR;///Clear WDT timer
    while((WDCON|~SET_BIT6)==0xFF);     //confirm WDT clear is ok before into power down mode
    set_WDTR;						    //WDT run
}

