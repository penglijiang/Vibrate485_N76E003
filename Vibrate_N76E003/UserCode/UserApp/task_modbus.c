/****************************************************************************************
  Copyright (C), 1988-1999, Huawei Tech. Co., Ltd.
  File name:      usart.c
  Author:         Harvard
  Version:        0.0
  Date: 		20070620
  Description:  完成串行口的初始化，设置，提供单字节的发送接收
  History: 
  ----------------------------------------------------------------------------------------    
  1. Date:		20070622
     Author:	XXJ
     Modification:
	 			t1.5与t3.5采用固定间隔代替，分别为3
  2. date:		20070707
  	 author:	xxj
	 abs:		顺利完成modbus的读参数功能,且调试时可以通过在代码中
	 			用串口发送不同状态码的方式来判定各种通信错误:如0位crc
				校验出错;1为通信地址错误.
				针对16位变量和8位变量在位地址处理上的异同.把所有的
				变量均统一成8位一个字节进行同一处理.只接收字地址寻址
				收到字地址后.立即乘以2,变成字节,对于16位变量,读两个
				字节恰好全部读取,而对于8位变量,可能将其他变量的8个位
				也读取了,组成一个字节,这样会造成每次读取时,由于其他
				变量的变化而每次的值都会不一样,解决办法时,尽可能将
				两个8位变量组合成一个16位的状态字,由用户去统一协调
				提取.还有一个办法是在message ,map 中,8位变量前面加入

*****************************************************************************************/
#include "config.h"

/*
 * 快读系统状态
 * 返回：系统的软件.硬件版本可用于系统的485硬件通断测试
 */

/* 函数名称：void mod_process(void)	
 * 输入:	 接收结束标志
 * 输出:
 */

 ///////////////////////////////////////////////
 u16 SYS_485_adr = 1 ;
 u16 SYS_485_baud ;/*Meaasga maps */
 static u08 Space = 0; 
 static u16 Space16 = 0; 

/* Modbus地址与实际内部变量的映射表 */ 
u08 * code modbus_tab[]   =	
{
      //只读
		&HIGH_(SYS_485_adr),                &LOW_(SYS_485_adr),	        //0: 本机地址       
		&HIGH_(SYS_x_data),                 &LOW_(SYS_x_data),	        //1: 本机地址 
		&HIGH_(SYS_y_data),		  	        &LOW_(SYS_y_data),	        //2
		&HIGH_(SYS_z_data),		  	        &LOW_(SYS_z_data),	        //3
		&HIGH_(SYS_485_adr),                &LOW_(SYS_485_adr),	        //4: 本机地址       
		&HIGH_(SYS_baudrate),               &LOW_(SYS_baudrate),	    //5: 本机地址       
		&HIGH_(Space16),		            &LOW_(Space16),	//3
		&HIGH_(Space16),		            &LOW_(Space16),	//4
		
        &HIGH_(Space16),		            &LOW_(Space16),	        //5
        &Space,		                        &LOW_(Space16),	//6
        &Space,		                        &LOW_(Space16),	//7
        &Space,		                        &LOW_(Space16 ),	//8
        &Space,		                        &LOW_(Space16 ),      //9
		
		
 };

/* 针对HLM_SOKO的需求, 构造特殊的传输帧,
*  数据经过0x10指令 ,类似于广播地址般的直接写入
*  从机 
*/ 
////////////////////////单路默认:左通道 
void modbus_wr( void ) 
{
    u08 i;
    //////////////////////////1-帧头 
    
    ////4 计算CRC ,后放在RxBuf[20..21]中

    CRC = 0xffff;
    for( i=0; i<20 ; i++ )
    {
        CRC = _crc16_update( CRC ,TxBuf[i] );
    }
    TxBuf[20] = CRC%256;
    TxBuf[21] = CRC/256;

    TxBufPtr = 21; 
    send_frame ();

}

////////////////////////单路默认:左通道 
void modbus_wr_R( void ) 
{
    u08 i;
    //////////////////////////1-帧头 
 
    
  
    
    ////4 计算CRC ,后放在RxBuf[20..21]中

    CRC = 0xffff;
    for( i=0; i<20 ; i++ )
    {
        CRC = _crc16_update( CRC ,TxBuf[i] );
    }
    TxBuf[20] = CRC%256;
    TxBuf[21] = CRC/256;
 
    
    TxBufPtr = 21; 
    send_frame ();

}

void mod_process(void)
{
	if( fFrameDone == OK )				//接收已经完成
	{
		fFrameDone = NO;	        	//清除标志

		usart_dis_recv();				//禁止接收中断

		if ( fFrameBad ==TRUE )     	//数据帧被破换，不予处理
		{
			 fFrameBad = FALSE;     	//清除错误标志
		}

		else 							//数据帧正常则解析协议
		{
			 cmd_proc();	
		}

			RxBufPtr   = RX_BUF_BGN_ADR;//准备下次接收

			usart_en_recv();	        //重新接收

	}
	
	if ( fRxBufFull == YES )   	   		//数据帧错误
	{
		 fFrameBad  =  FALSE;     		//清除错误标志

         RxBufPtr   =  RX_BUF_BGN_ADR;	//准备下次接收
	} 
		 fRxBufFull =  NO	  ;     	//清空缓冲区

		 usart_en_recv();	          	//重新接收
 
}

/*命令处理*/
void cmd_proc(void)
{
	u16 reg_adr =0; //临时 存放要读写的寄存器的地址
	u08 reg_errcode =0;//出错时的错误编码  

	////////////////////////////////////////////=======================CRC 校验		
	if ( CRC != 0 )                            
	{
		//usart_send_byte(0);	   //错误代码0: crc 校验错误
		return ;
	}
		CRC = 0xffff;	//准备计算crc	
		
	///////////////////////////////////////////========================判断地址是否正确
	if( SYS_485_adr != RxBuf[0] && 0 != RxBuf[0] )
	{
		return ;
	}
 
	
		TxBuf[0] = SYS_485_adr ;//respond frame's header
		TxBuf[1] = RxBuf[1];//respond frame's function code			
		CRC      = _crc16_update( CRC ,TxBuf[0] );
		CRC      = _crc16_update( CRC ,TxBuf[1] );
	////////////////////////////////////////// ========================识别功能码
	if( RxBuf[1]==0x07 )                      //----------------fast read version
	{
		TxBuf[2]= (HVERSION<<4)+SVERSION;
		CRC = _crc16_update( CRC ,TxBuf[2] );
		TxBuf[3] = CRC%256;
		TxBuf[4] = CRC/256;
		TxBufPtr = 4; 
		send_frame ();
	}
	else 
	if( RxBuf[1]==0x03 || RxBuf[1]== 0x04 )	 //-------------------------读寄存器
	{
		u08  cnt;
 			 TxBuf[2]   = RxBuf[5]*2;			 //字节数
			 CRC 		= _crc16_update( CRC ,TxBuf[2] );
 			 TxBufPtr   = 3;		   //调整发送指针，
		for( cnt =0 ; cnt< TxBuf[2] ; )//RxBuf[5]--word counts
		{
		    TxBuf[TxBufPtr] = *((u08 *) modbus_tab [cnt +RxBuf[3]*2]);//RxBuf[3]中为起始地址低8位
			CRC         = _crc16_update( CRC ,TxBuf[TxBufPtr] );
			cnt         =  cnt + 1;
			TxBufPtr    =  TxBufPtr + 1  ;
		}
			TxBuf[TxBufPtr] = CRC%256;
			TxBufPtr        = TxBufPtr + 1;	//调整发送指针
			TxBuf[TxBufPtr] = CRC/256;
			send_frame();//发送	 
	 }
	else 
	if( RxBuf[1]==0x10 )					 //--------------------写寄存器
	{
		u08  cnt;
		     TxBuf[2]   = RxBuf[2];	  		//写起始地址高8位，一般为0
		     TxBuf[3]   = RxBuf[3];			//写起始地址低8位
		     TxBuf[4]   = RxBuf[4];	  		//欲写寄存器数目高8位，一般为0
		     TxBuf[5]   = RxBuf[5];			//欲写寄存器数目低8位
	 
	 //不允许写长度为0
	 if(RxBuf[6] < 2)	 goto err_ret;		
	//通信协议的规范性检测，从数据帧长度判断	
	if( (6 + 2 +  RxBuf[6] )  != RxBufPtr )  goto err_ret;//指令格式不正确

	//是否超出通信协议的地址表范围，所有参数共26字节	
	 if( (  RxBuf[5] + RxBuf[3]  )  >20 )    goto err_ret;//字地址及数目超范围
	// 20070713 ：考虑安全性，将11 12号地址的写功能关闭
 
    //
	// if( (RxBuf[3] <= 9 ))					 goto err_ret;//10号子地址以下为只读
	 
		for( cnt =0 ; cnt< RxBuf[6] ;cnt++ )     //RxBuf[6]--byte counts
		{
	      
		  *((u08 *) modbus_tab[ cnt +RxBuf[3]*2] )
		    
			= 
			
			RxBuf[ 7 + cnt ];	    //RxBuf[3]中为起始地址低8位，乘以2，是将之转换为内部的字节地址

		}

		for ( cnt = 0 ; cnt < 4 ; cnt ++ ) //发送字节指针为0-7，其中：0 .1 为固定，其crc已经计算好
		{		
            CRC = _crc16_update( CRC ,TxBuf[2+cnt] );//2-5
		}
			TxBuf[6] = CRC%256;
			TxBuf[7] = CRC/256;	 			
			TxBufPtr = 7;		//调整发送指针
			send_frame();		//发送	
			 
			//SynE2promFromRam();   //保存更改

		    usart_init (SYS_485_baud);//重新初始化串行口
			//通信参数的写操作功能已经被关闭

			err_ret: 	TxBufPtr = TX_BUF_BGN_ADR;    //after sending ,reset ptr
	}
    
    
    else 
	if( RxBuf[1]==0x06 )					//----------写寄存器(单个寄存器)
	{
		/* 先获取寄存器地址 */
		reg_adr =((u16) RxBuf[2]<<8) + (u16) RxBuf[3]	;	//	reg_adr = (u16) RxBuf[3]	;
 
        /* 最大写入地址限定 */
        if(reg_adr > MODBUS_ADDR_MAX )	
		{
			reg_errcode = 2;//非法的数据地址
			
			goto err_ret_06;
		}
		/* 预写一次寄存器值
	    */
		 *((u08 *)(modbus_tab [reg_adr*2]))=  RxBuf[4] ;
		 *((u08 *)(modbus_tab [reg_adr*2+1]))=RxBuf[5] ;
         
        ///////发送会送数据帧 
        TxBuf[0] = RxBuf[0];
        TxBuf[1] = RxBuf[1];
        TxBuf[2] = RxBuf[2];
        TxBuf[3] = RxBuf[3];
        TxBuf[4] = RxBuf[4];
        TxBuf[5] = RxBuf[5];
        TxBuf[6] = RxBuf[6];
        TxBuf[7] = RxBuf[7];
        TxBufPtr = 7;		//调整发送指针
        send_frame();		//发送				
 			
        ///////保存参数 
        //save_user_data_to_flash();   //保存更改
        
        /////////////错误处理:
        err_ret_06: 
        /* 如果是非法的数据地址 */
        if( reg_errcode == 2 )
        {
            TxBuf[0] = RxBuf[0];
            TxBuf[1] = 0x86;
            CRC =0xffff;
            CRC      = _crc16_update( CRC ,TxBuf[0] );
            CRC      = _crc16_update( CRC ,TxBuf[1] );
            TxBuf[2] = reg_errcode;
            CRC      = _crc16_update( CRC ,TxBuf[2] );
            TxBuf[3] = CRC%256;
            TxBuf[4] = CRC/256;
            TxBufPtr = 4;
            send_frame();
        }
        else 
        if( reg_errcode == 3 )
        {
            TxBuf[0] = RxBuf[0];
            TxBuf[1] = 0x86;
            CRC =0xffff;
            CRC      = _crc16_update( CRC ,TxBuf[0] );
            CRC      = _crc16_update( CRC ,TxBuf[1] );
            TxBuf[2] = reg_errcode;
            CRC      = _crc16_update( CRC ,TxBuf[2] );
            TxBuf[3] = CRC%256;
            TxBuf[4] = CRC/256;
            TxBufPtr = 4;
            send_frame();
        } 
            
			TxBufPtr = TX_BUF_BGN_ADR;    //after sending ,reset ptr         
    }


}

/*发送一一帧数据*/
void send_frame(void)
{
	u08  i;
	for( i = 0 ; i <= TxBufPtr; i++ ) //TxBufPtr为发送缓冲区指示器
	{
		usart_send_byte(TxBuf[i]);
	}
		TxBufPtr = TX_BUF_BGN_ADR;    //after sending ,reset ptr
}
