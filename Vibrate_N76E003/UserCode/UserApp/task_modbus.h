/****************************************************************************************
  Copyright (C), 2007-2999, SinLei Tech. Co., Ltd.
  File name:      ModbusApp.h
  Author:         Harvard
  Version:        0.0
  Date: 		  20070623
  Description:    prototype for modbus protocol
  ----------------------------------------------------------------------------------------    
  1. Date:		
     Author:	
     Modification:
  ----------------------------------------------------------------------------------------    
*****************************************************************************************/
#ifndef _MODBUSAPP_H
#define _MODBUSAPP_H

///////////MACROS
#define T15	 5             /*1.5字符时间，从简处理不因波特率而改变.约为3ms*/
#define T35 5			  /*3.5字符时间，从简处理不因波特率而改变.约为7ms*/

  
void modbus_wr       (void) ;
void modbus_wr_R       (void) ;
void mod_process     (void) ;
void cmd_proc        (void) ;/**/
void send_frame      (void) ;/*发送数据帧*/

#define MODBUS_ADDR_MAX 80         
                                

 
#endif


