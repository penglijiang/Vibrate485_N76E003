//*****************************************************************//
//filename: cpu.c
//author:   xxj
//date:		2006.03.30
// 20091113: 提高软定时器的定时精度.各个定时器的1s源独立开,t0_cnt_1s
//			改为t0_cnt_1s[]数组.防止1s定时太短.引起电机突然换向,导致
//			故障.
// -----------------------------------------------------------------
//*****************************************************************//
 
#include "config.h"

struct TIMER Timer[TIMER_N];


//*****************************************************************//
//functionname:     void  TimerInit(void);
//purpose:		 initialize	timer by user
//input:
//output
//call:
//becalled;
//date:			2006.04.13
//created:		xxj
//*******************************************************************
void  Timer0Init(void)
{
  /*t0 initailize*/
 
}

void  Timer2Init(void)
{

}

/**************************************************************
* functionname: signal(sig_overflow0)
***************************************************************/

//定时器0相关的控制变量
//t0
// u08 	second	=	0 ;
// u08 	minute	=	0 ;
// u08  	hour  	=	0 ;
//基础软件定时器- 秒 分
u16 t0_cnt_20ms = 0;	//20ms定时器软计数器
u16 t0_cnt_10ms = 0;	//20ms定时器软计数器


//u16		t0_cnt_1s 	= 0;	//1s定时器软计数器
u16 t0_1s       =0 ;
u16 t0_500ms    =0 ;
u16 t0_cnt_1s[TIMER_N] 	={0,};	//1s定时器软计数器
    
u16 t0_cnt_1s_base[TIMER_N] 	=
{
    1000,	 //timer0	 x电机

};	//1s定时器时间基准调整 


u16		t0_cnt_1min = 0;	//20ms定时器软计数器
u16		t0_cnt_1hour = 0;	//20ms定时器软计数器

u08		f20ms 	= false ;	//定时器到标志
u08		f10ms 	= false ;	//定时器到标志
u08		f500ms 	= false ;	//定时器到标志
u08		f1s 	= false ;	//定时器到标志
u08		f1min 	= false ;	//定时器到标志
u08		f1hour 	= false ;	//定时器到标志

/*********定时器0的中断，来产生系统工作的基本时序**************/
//void nothing(void)

/**
 * @brief       Timer-0 IRQ
 *
 * @param       None
 *
 * @return      None
 *
 * @details     The TIMER0 default IRQ, declared in startup_M051Series.s.
 */
 
void Timer0_ISR (void) interrupt 1 
{ 
    u08 i;

    //////////////////////////////////////////////--重新加载定时器初值
     /* 给TIMER 赋初值 
     ** 定时器时钟为:16MHZ/12= 4/3MHZ
     ** 定时器tick为: 1/ (4/3MHZ) = 3/4us = 0.75uS
     ** 定时器要定时1ms:
     ** 定时器初值为: 1000us/0.75uS = 1333
     */
    TH0 = (65535-1333)/256;
    TL0 = (65535-1333)%256;

     //各个软定时器处/////////////////////////////////////////////////
	 //10ms定时到=====================================================
	  if( t0_cnt_10ms++  > 5 )  
	 {
	 	  t0_cnt_10ms = 0;
		  
		  f10ms		  = true; 
	 }  
	 //20ms定时到=====================================================
	  if( t0_cnt_20ms++  > 10 )  
	 {
	 	  t0_cnt_20ms = 0;
		  
		  f20ms		  = true; 
	 }
     
	  if( t0_500ms++   > 500 )  
	{
	 	  t0_500ms = 0;
		  f500ms  = true;
	}     
  //1s级定时到=====================================================
 
 	//本标记用于温度的1s采集
	  if( t0_1s++   > 1000 )  
	{
        t0_1s = 0;
        f1s  = true;
        LED_proc();

	}
		//////////////////////////////////////////
		//倒计时定时器处理
//		for(   i =0 ; i< TIMER_N; i++)
//		{
//			/* 软定时器1s到*/
//			if(	t0_cnt_1s[ i ]++ >=	t0_cnt_1s_base[i]	)
//			{
//			    t0_cnt_1s[ i ] 	=	0;
//				//////////////////////////////////////
//				if( Timer[i].TR == ON )
//				{
//					//////////////////////////////////
//					if( Timer[i].second-- == 0 )
//					{
//						//////////////////////////////
//						if( Timer[i].minute-- > 0)
//						{
//							Timer[i].second = 60;
//						}
//						else
//						{
//							Timer[i].TF = true;	
//						}
//						//////////////////////////////
//					}/////////////////////////////////
//				}/////////////////////////////////////
//			}
//			
//		}
		//倒计时定时器处理
		//////////////////////////////////////////

}

/**************************************************************************************
** 功能：	打开定时器。 复位定时器的时候可以重复打开该定时器
** 说明：	函数为定时到时，需要执行的函数
**			复位各标志位 寄存器 所有的秒针变动在中断服务中触发
***************************************************************************************/

void set_on_timer( u08 timer ,u08 minute,u08 second, FUNPTR_VOID_VOID fptr )
{
	//定时器合法性检查
    cli();//关闭中断

	if( timer >=  TIMER_N)
	{
		timer = TMR0;
	}
	t0_cnt_1s[ timer] =0   ;	//复位被定时器的1s源
	//定时器控制变量
	Timer[ timer ].TR = ON ;	//开定时器
	
	Timer[ timer ].TF = OFF ;	//清除标志位

	
//	if( second >= 0 )			//注意=0不可移除。会导致分计数器赋值失败。导致倒计时出现异常
	{
		Timer[ timer ].second = second ;	//定时器秒赋值
	
	}
		Timer[ timer ].minute = minute  ;	//定时器分赋值

	//定时到 所要执行的函数
   	Timer[ timer ].funptr = fptr;

    sei();//kai中断

}
/**************************************************************************************
** 关闭某个定时器
***************************************************************************************/
void set_off_timer( u08 timer  )
{
    cli();//关闭中断

	//定时器合法性检查
	if( timer >=  TIMER_N)
	{
		timer = TMR0 ;
	}
	t0_cnt_1s[ timer] =0   ;	//复位被定时器的1s源
	//定时器控制变量
	Timer[ timer ].TR = OFF ;		//开定时器
	
	Timer[ timer ].TF = OFF ;		//清除标志位

	Timer[ timer ].minute = 0x00 ;	//内部定时器清零

	Timer[ timer ].second = 0x00 ;	//内部定时器清零
	
	//定时到 所要执行的函数
   	Timer[ timer ].funptr = NULL;

    sei();//开中断
}
/**************************************************************************************
** 函数名：	定时器溢出函数处理
** 功能：	更具定时器的溢出标志执行对应的功能
** 注意：	一般放在后台程序中运行。
***************************************************************************************/
void on_timer(void)
{   u08 i =0;
	for(  i =0 ; i< TIMER_N; i++ )
	{
        
        if( Timer[i]. TR == ON )///////////////加入TR控制 防止重复响应
        {

    		if(	Timer[i].TF == true)
    		{
                cli();

                Timer[i].TR = OFF;

    			Timer[i].TF = false;	//防止重复执行

                sei();
    
    			if( Timer[i].funptr != NULL )
    			{
     				(* Timer[i].funptr)();
    			}
    		}
        }//////////////////////////////////////20091208
	}
}
//////////////////////////////////===1 定时器溢出函数
    /** 测试循环 */


void _delay_us( u16 n_us )
{
    u08 i =0;u16 j=0;
    for( j=0; j<n_us;j++)
    {
        for( i=0;i<2;i++)
        {
           _nop_();
        }
    }
}


void _delay_ms( uint32_t n_ms )
{
  uint32_t i,j;
    for(  i=0 ; i < n_ms; i++ )
    {
        for(  j=0 ; j < 1000; j++ )
        {
            _delay_us( 1 );
        }
     }
} 


//由于_delay_ms函数有所限制，必须控制每次延时的最大值，
void _delay_100ms( void )
{
	_delay_ms(10);	_delay_ms(10); 	_delay_ms(10); 	_delay_ms(10);	_delay_ms(10);
	wdt_reset();
	_delay_ms(10); 	_delay_ms(10);_delay_ms(10);	_delay_ms(10);	_delay_ms(10);
}
void _delay_1s( void )
{
    _delay_100ms(); _delay_100ms(); _delay_100ms(); _delay_100ms(); _delay_100ms();
    _delay_100ms(); _delay_100ms(); _delay_100ms(); _delay_100ms();  
}
void _delay_500ms( void )
{
    _delay_100ms(); _delay_100ms(); _delay_100ms(); _delay_100ms(); _delay_100ms();
}

