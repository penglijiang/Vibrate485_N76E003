#ifndef _VIBRATE_H
#define _VIBRATE_H

/*********** APP重要参数配置入口  ***********/

#define WORK_LED            P04///工作指示灯: 正常-常亮; 异常-闪烁
#define TXD_EN              P05
#define ADXL345_SCLK        P10///ADXL345的SPI接口定义
#define ADXL345_SDO         P00
#define ADXL345_SDI         P01
#define ADXL345_CS          P11
#define ADXL345_INT1        P30
#define ADXL345_INT2        P17

#define LED_ON              0//定义led开的电平
#define LED_OFF             1//定义led开的电平


/***********  项目功能函数声明 ***********/
void delay_us( u16 n_us );
void delay_ms( u16 n_ms );




#endif

