/*---------------------------------------------------------------------------------------------------------*/
/*                                                                                                         */
/* Copyright(c) 2017 Nuvoton Technology Corp. All rights reserved.                                         */
/*                                                                                                         */
/*---------------------------------------------------------------------------------------------------------*/
 
//***********************************************************************************************************
//  File Function:  振动测量主程序
//***********************************************************************************************************

/* Includes ------------------------------------------------------------------*/
#include "config.h"

u08 eeprom[128];


/*******************************************************************************
 * FUNCTION_PURPOSE: Main function 
 ******************************************************************************/
void main (void)
{
    /* 系统初始化*/
    SYS_init();
    TMR0_Init();
    TMR1_Init();
    TMR2_Init();
    
    /* 初始化串口 */
    InitialUART0_Timer3(9600);
    //SPI_Initial();
    
    /* 看门狗初始化 */
    WDT_Initial();
    
    /*  开中断 */
    sei();
    
    /* 测试程序 */

    
    /* APP程序初始化 */
    LED_operate (_LED1_, ON);///点亮工作指示灯;
    LED_flash   (_LED1_, ON);
    
    /*  初始化adxl345 */
    
    SYS_adxl_buf[0] = 0x0b;
    WriteToADXL345ViaSpi(XL345_DATA_FORMAT,1,SYS_adxl_buf);
    
    SYS_adxl_buf[0] = 0x08;
    WriteToADXL345ViaSpi(XL345_POWER_CTL,1,SYS_adxl_buf);
    
    ReadFromADXL345ViaSpi(XL345_DEVID,1,SYS_adxl_buf);
    
    SYS_adxl345_deviceid = SYS_adxl_buf[0];
    /****** 主循环 *******/
    while(1)
    {
        if( f20ms)
        {
            f20ms =0;
            
            ReadFromADXL345ViaSpi(XL345_DATAX1,1,SYS_adxl_buf);
           SYS_x_data = SYS_adxl_buf[0];
           SYS_x_data = SYS_x_data<<8;
            ReadFromADXL345ViaSpi(XL345_DATAX0,1,SYS_adxl_buf);
           SYS_x_data = SYS_x_data +SYS_adxl_buf[0];
           
            ReadFromADXL345ViaSpi(XL345_DATAY1,1,SYS_adxl_buf);
           SYS_y_data = SYS_adxl_buf[0];
           SYS_y_data = SYS_y_data<<8;
            ReadFromADXL345ViaSpi(XL345_DATAY0,1,SYS_adxl_buf);
           SYS_y_data = SYS_y_data +SYS_adxl_buf[0];
           
                      
             ReadFromADXL345ViaSpi(XL345_DATAZ1,1,SYS_adxl_buf);
           SYS_z_data = SYS_adxl_buf[0];
           SYS_z_data = SYS_z_data<<8;
            ReadFromADXL345ViaSpi(XL345_DATAZ0,1,SYS_adxl_buf);
           SYS_z_data = SYS_z_data +SYS_adxl_buf[0];
           
                     
        
        }
    
        /* 3- MODBUS_RTU通信处理 */
        mod_process();
    
        /* x-喂狗 */
        WDT_reset();
    }
    /****** 主循环 *******/
	
		
}


  
