/**
  ******************************************************************************
  * @file    Initialization.c
  * @author  Shine
  * @version V1.0
  * @date    2017-06-29
  * @brief   系统的初始化,底层操作,类似于BSP
  ******************************************************************************
  * @attention
  *
  *
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/
#include "config.h"

/*
** 系统所有的定时器初始化;
*/

void TMR0_Init(void)
{
    TIMER0_MODE1_ENABLE;////设置定时器0为模式1: 16bit模式 
    clr_T0M ;           ///设定定时器0为系统时钟的1/12
     
     /* 给TIMER 赋初值 
     ** 定时器时钟为:16MHZ/12= 4/3MHZ
     ** 定时器tick为: 1/ (4/3MHZ) = 3/4us = 0.75uS
     ** 定时器要定时1ms:
     ** 定时器初值为: 1000us/0.75uS = 1333
     */
    TH0 = (65535-1333)/256;
    TL0 = (65535-1333)%256;
    set_ET0;            ///enable Timer0 interrupt
    set_TR0;            ///Timer0 run
}


void TMR1_Init(void)
{
    TIMER1_MODE1_ENABLE;////设置定时器0为模式1: 16bit模式 
    clr_T1M ;           ///设定定时器0为系统时钟的1/12
     
     /* 给TIMER 赋初值 
     ** 定时器时钟为:16MHZ/12= 4/3MHZ
     ** 定时器tick为: 1/ (4/3MHZ) = 3/4us = 0.75uS
     ** 定时器要定时1ms:
     ** 定时器初值为: 1000us/0.75uS = 1333
     */
    TH1 = (65535-1333)/256;
    TL1 = (65535-1333)%256;
    set_ET1;            ///enable Timer0 interrupt
    set_TR1;            ///Timer0 run
}

void TMR2_Init(void)
{

}
void TMR3_Init(void)
{

}


void SYS_init( void )
{
    /* 1- 系统时钟初始化  */
      
    
    /* 2- IO 初始化  */
    /*先设定所有的IO为准双向*/
    Set_All_GPIO_Quasi_Mode;
    /*设定LED引脚为推拉模式*/
    P12_PushPull_Mode;
    P05_PushPull_Mode;

}








