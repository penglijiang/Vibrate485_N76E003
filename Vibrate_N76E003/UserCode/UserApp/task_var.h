/********************************************************************************
** file name : task_var.h
** utility   : ad采集模块中的函数声明以及常数定义 
** ------------------------------------------------------------------------------
*********************************************************************************/
#ifndef _TASK_VAR_H
#define	_TASK_VAR_H

/* 定义2个变量,用于led灯开关,闪烁 */
extern u08 SYS_led_flash   ;
extern u08 SYS_led   ;

extern u08 SYS_led1  ;
extern u08 SYS_led2  ;
extern u08 SYS_led3  ;
extern u08 SYS_led4  ;

extern u08 SYS_led1_flash ;
extern u08 SYS_led2_flash ;
extern u08 SYS_led3_flash ;
extern u08 SYS_led4_flash ;

extern int16 SYS_x_data   ;///XYZ三轴加速度 +-32768 分辨率:3.9mg/LSB
extern int16 SYS_y_data   ;
extern int16 SYS_z_data   ;
extern u16 SYS_baudrate   ;

extern u16 SYS_adxl345_deviceid;
extern unsigned char SYS_adxl_buf[4]  ;

#endif
